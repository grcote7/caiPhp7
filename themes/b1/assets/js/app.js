$(function() {
	new WOW().init();

	// $(".navbar").removeClass("navbar-expand-sm");
	// $(".navbar").addClass("navbar-expand-md");

	var userMenus = function() {
		if ($("#navbarsExample03").is(":hidden")) {
			$("#userMenu1")
				.fadeIn("slow")
				.css("display", "inline-block");
			$("#userMenu2")
				.fadeOut("slow")
				.css("display", "none");
		} else {
			$("#userMenu1")
				.fadeOut("slow")
				.css("display", "none");
			$("#userMenu2")
				.fadeIn("slow")
				.css("display", "inline-block");
		}
	};

	$(window).resize(function() {
		userMenus();
	});

	userMenus();
	console.log("B Ready !");
});
