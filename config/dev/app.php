<?php

/*
 * (c) Boosteur.com - 2020
 */

return [
  'app_local' => 789,
  'debug'     => env('APP_DEBUG', true),
  'debugAjax' => true,
];