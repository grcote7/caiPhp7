$(document).ready(function () {

  // $(function () {
  //   $('#folder-1').click(function () {
  //     toastr.error("Folder 1 has been clicked!", "Folder 1", {
  //         "positionClass": "toast-top-right",
  //     });
  //   });
  //   $('#folder-2').click(function () {
  //     // make it not dissappear
  //     toastr.info("Folder 2 has been clicked!", "Folder 2", );
  //   });
  //   $('#folder-3').click(function () {
  //     // make it not dissappear
  //     toastr.success("Folder 3 has been clicked!", "Folder 3", );
  //   });
  // });


  // Initializations

  // Material Select Initialization
  $(document).ready(function() {
    $('.mdb-select').material_select();
  });
  
  // Charts

  // Minimalist chart
  $(function() {
    $('.min-chart#chart-sales').easyPieChart({
      barColor: "#4caf50",
      onStep: function(from, to, percent) {
        $(this.el).find('.percent').text(Math.round(percent));
      }
    });
  });

});
