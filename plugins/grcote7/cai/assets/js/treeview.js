$(document).ready(function() {
  
  (function(e) {
		e.fn.mdbTreeview = function() {
			var t = e(this);
			if (t.hasClass("treeview")) {
				var n = t.find(".rotate");
				e.each(n, function(t) {
					e(n[t]).off("click"),
						e(n[t]).on("click", function() {
							var t = e(this);
							t.siblings(".nested").toggleClass("active"),
								t.toggleClass("down");
						});
				});
			}
			if (t.hasClass("treeview-animated")) {
				var i = t.find(".treeview-animated-element"),
					r = t.find(".closed");
				t.find(".nested").hide(),
					r.off("click"),
					r.on("click", function() {
						var t = e(this),
							n = t.siblings(".nested"),
							i = t.children(".fa-angle-right");
						return (
							t.toggleClass("open"),
							i.toggleClass("down"),
							n.hasClass("active")
								? n.removeClass("active").slideUp()
								: n.addClass("active").slideDown(),
							!1
						);
					}),
					i.off("click"),
					i.on("click", function() {
						var t = e(this);
						t.hasClass("opened")
							? t.removeClass("opened")
							: (i.removeClass("opened"), t.addClass("opened"));
					});
			}
			if (t.hasClass("treeview-colorful")) {
				var o = t.find(".treeview-colorful-element"),
					a = t.find(".treeview-colorful-items-header");
				t.find(".nested").hide(),
					a.off("click"),
					a.on("click", function() {
						var t = e(this),
							n = t.siblings(".nested"),
							i = t.children(".fa-plus-circle"),
							r = t.children(".fa-minus-circle");
						t.toggleClass("open"),
							i.removeClass("fa-plus-circle"),
							i.addClass("fa-minus-circle"),
							r.removeClass("fa-minus-circle"),
							r.addClass("fa-plus-circle"),
							n.hasClass("active")
								? n.removeClass("active").slideUp()
								: n.addClass("active").slideDown();
					}),
					o.off("click"),
					o.on("click", function() {
						var t = e(this);
						t.hasClass("opened")
							? o.removeClass("opened")
							: (o.removeClass("opened"), t.addClass("opened"));
					});
			}
		};
	})(jQuery),
  
  $('.treeview-colorful').mdbTreeview();
});
