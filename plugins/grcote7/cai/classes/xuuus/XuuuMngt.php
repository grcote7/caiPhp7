<?php

/*
 * (c) Boosteur.com - 2020
 */

namespace Grcote7\Cai\Classes;

use Grcote7\Cai\Classes\Xuuus\U;
use Illuminate\Support\Facades\DB;

class XuuuMngt
{
  public function __construct($table)
  {
    $this->table = $table;
  }

  public function getParr($u)
  {
    return Db::table($this->table)
      ->where($this->table.'.uname', $u->parr)
      ->first();
  }

  public function getFill($u)
  {
    return Db::table($this->table)
      ->where($this->table.'.parr', $u->uname)
      ->get();
  }

  public function getGr($u)
  {
    // $gr[$u->uname] = [$u];

    $ssgr = $this->getFill($u);
    if (count($ssgr)) {
      $this->gr[$u->uname] = $ssgr;
      
      foreach ($this->gr[$u->uname] as $fill) {
        $this->grr[$u->uname] = $this->getGr($fill);
      }
    }

    return $this->gr;
  }
}