<?php

/*
 * (c) Boosteur.com - 2020
 */

namespace Grcote7\Cai\Models\Xu;

use Model;
use Grcote7\Cai\Classes\Xuuus\U;

/**
 * Model.
 */
class Xu extends Model implements U
{
  use \October\Rain\Database\Traits\Validation;

  /*
   * Disable timestamps by default.
   * Remove this line if timestamps are defined in the database table.
   */
  public $timestamps = false;

  /**
   * @var string The database table used by the model.
   */
  public $table = 'xuuus';

  /**
   * @var array Validation rules
   */
  public $rules = [
  ];
}