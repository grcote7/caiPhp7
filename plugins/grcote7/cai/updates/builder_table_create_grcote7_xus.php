<?php

/*
 * (c) Boosteur.com - 2020
 */

namespace Grcote7\Cai\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class builder_table_create_grcote7_xus extends Migration
{
  public function up()
  {
    Schema::create('grcote7_xus', function ($table) {
      $table->engine = 'InnoDB';
      $table->bigIncrements('id')->unsigned();
      $table->string('uname');

      $table->bigInteger('bg')->unsigned()->nullable();
      $table->bigInteger('bd')->unsigned()->nullable();
      $table->integer('prof')->unsigned()->nullable();

      $table->string('parr');
      $table->string('tut');
      $table->smallInteger('lv')->unsigned();
      $table->string('type');
      $table->smallInteger('lva');
      $table->smallInteger('lvp');

      $table->string('parrori');
      $table->bigInteger('transf')->unsigned();
    });
  }

  public function down()
  {
    Schema::dropIfExists('grcote7_xus');
  }
}