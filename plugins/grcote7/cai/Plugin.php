<?php

/*
 * (c) Boosteur.com - 2020
 */

namespace Grcote7\Cai;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
  public function registerComponents()
  {
    return [
      'Grcote7\Cai\Components\Wwwboos'    => 'wwwboos',
      'Grcote7\Cai\Components\Cai'        => 'cai',
      'Grcote7\Cai\Components\Group'      => 'group',
      'Grcote7\Cai\Components\Groupview0' => 'groupview0',
      'Grcote7\Cai\Components\U'          => 'u',
      'Grcote7\Cai\Components\Uu'         => 'uu',
    ];
  }

  public function registerSettings()
  {
  }
}