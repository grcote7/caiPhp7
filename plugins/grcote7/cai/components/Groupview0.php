<?php

/*
 * (c) Boosteur.com - 2020
 */

namespace Grcote7\Cai\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\DB;
use RainLab\User\Facades\Auth;
use stdClass;

class GroupView0 extends ComponentBase
{
  public $var;

  public function componentDetails()
  {
    return [
      'name'        => 'u',
      'description' => 'Manipes Users',
    ];
  }

  public function onRun()
  {
    $this->addJs('assets/js/treeview.js');

    ini_set('max_execution_time', 3600);
    error_reporting(E_ALL);
    // \Debugbar::enable();
    // $this->addCss('assets/css/dev.css');

    $id = Auth::getUser()->id;
    // $id = 99;

    // $this->var = $this->getCa();
    $this->var = $this->getGroupStatus($id);
    // return $this->getGroupStatus($id);
  }

  public function getActivGroup($id)
  {
    $db = 'grcote7_xus';

    DB::select('select bg, bd, prof from '.$db.' where id=? into @bg, @bd, @prof', [$id]);

    $req = ' where bg >= @bg and bd <= @bd ';
    $us  = DB::select('select count(*) as nb from '.$db.$req.' and (prof - @prof<33333) order by bg');

    // info($us[0]->nb);

    // return $us[0]->nb;

    $vars['tout']          = new stdClass();
    $vars['tout']->nbr     = 12345;
    $vars['tout']->actifs  = 12345;
    $vars['tout']->pct     = 12.45;
    $vars['prof3']         = new stdClass();
    $vars['prof3']->nbr    = 1234;
    $vars['prof3']->actifs = 1234;
    $vars['prof3']->pct    = 12.4;

    // echo '<main><div class="row"><div class="col mx-1">';
    // var_dump($vars);
    // echo '</div></div></main>';
    // return $vars;
    $this->var = $vars;
    // $this->var = 2;

  // return $us;
  /*
  DB::select('select bg, bd, prof from '.$db.' where id=? into @bg, @bd, @prof', [1]);
  $req = ' where bg >= @bg and bd <= @bd ';

  $us = DB::select('select x.id, @prof as profini, uname, email, last_login, x.prof, parr from '.$db.' x	LEFT OUTER JOIN  users u
  ON x.id = u.id
  '.$req.' and (x.prof - @prof<3) order by bg');
  */
  // $email = 'dudu@cote7.com';
  // $parts = explode('@', $email);
  // return $parts[0];
/*
  $parts0 = [];

  foreach ($us as $k => $u) {
  $email    = $u->email;
  $parts    = explode('@', $email);
  $part     = $parts[0];
  $parts0[] = $u->id.' '.$part;

  // DB::update('update grcote7_boos_xus set email="'.$part.'@cote7.com" where email like "'.$part.'@clubadel.com%"');

  // echo $k.' : '.$email.' ('.$parts[0].')<br>';
  }

  $tab2 = array_unique($parts0);
  // echo count($parts0);

  return $parts0;
  // return $us;
  */
  }

  public function getGroupStatus($id)
  {
    $db = 'grcote7_xus';
    DB::select('select id, bg, bd, prof from '.$db.' where id=? into @id, @bg, @bd, @prof', [$id]);

    $u = DB::select('select @bg bg, @bd bd');

    // dd($u[0]->bd - $u[0]->bg - 1);
    // dd($u[0]);
    if ($u[0]->bd - $u[0]->bg - 1) {
      $cas = DB::select('
    
    SELECT 
      @tous:=(select COUNT(*) FROM grcote7_xus
      WHERE  bg > @bg and bd < @bd) AS nbr,

      @actifs:=(select COUNT(*) FROM grcote7_xus  x
        LEFT OUTER JOIN users u
        ON u.id = x.id
      WHERE  bg > @bg and bd < @bd
      AND u.last_login IS not NULL) as actifs,
	
    	ROUND((@actifs/@Tous)*100,2) AS pct
	
    union
	
    SELECT
      @3prof:=(select COUNT(*) FROM grcote7_xus
      WHERE  bg > @bg and bd < @bd
      and prof-@prof <3) AS 3prof,
    
      @actifs_3prof:=(select COUNT(*) FROM grcote7_xus  x
        LEFT OUTER JOIN users u
        ON u.id = x.id
      WHERE  bg > @bg and bd < @bd
      AND u.last_login IS not NULL
      AND x.prof-@prof <3),
    
      ROUND((@actifs_3prof/@3prof)*100,2)
    
    union

    select
      @parrori:=(select COUNT(*) FROM grcote7_xus
      WHERE  parrori = @id) AS parrori,
      
      @actifs_parrori:=(select COUNT(*) FROM grcote7_xus  x
        LEFT OUTER JOIN users u
        ON u.id = x.id
      WHERE  parrori = @id
      AND u.last_login IS not NULL),
      
      ROUND((@actifs_parrori/@parrori)*100,2)
;');

      $cas[0]->badge   = 'Tout le Groupe';
      $cas[0]->tooltip = 'Pourcentage des comptes actifs dans tout votre réseau';
      if (\array_key_exists(1, $cas)) {
        $cas[1]->badge   = '3 profondeurs';
        $cas[1]->tooltip = 'Pourcentage des comptes actifs dans les 3 premières profondeurs de votre réseau';
      }
      if (\array_key_exists(2, $cas)) {
        $cas[2]->badge   = 'Cooptation perso';
        $cas[2]->tooltip = 'Pourcentage des comptes actifs parmis ceux que vous avez personnellement coopté';
      }
      // foreach ($cas as $k => $c) {
      //   echo $k.' : '.$c->tooltip.'<br>';
      // }
      return $cas;
    }
  }

  public function affGroup($id)
  {
    $gr = $this->getGroup($id);

    $affGr   = [];
    $affGr[] = '<table>';

    foreach ($gr as $u) {
      $affGr[] = '<tr><td>'.$u->prof.'</td><td>'.str_repeat('&nbsp;', (($u->prof - $u->profini) * 7)).' '.$u->uname.'</td><td>'.$u->email.'</td><td>'.(($u->last_login) ? 'Yes' : 'no').'</td></tr>';
    }
    $affGr[] = '</table>';

    return implode('', $affGr);
  }

  public function getGroup($id)
  {
    $db = 'grcote7_xus';

    DB::select('select bg, bd, prof from '.$db.' where id=? into @bg, @bd, @prof', [$id]);

    $req = ' where bg >= @bg and bd <= @bd ';

    return DB::select('select @prof as profini, uname, email, last_login, x.prof, parr from '.$db.' x	LEFT OUTER JOIN  users u
    ON x.id = u.id
  '.$req.' and (x.prof - @prof<4) order by bg');
  }

  public function getPeres($id)
  {
    $db = 'grcote7_xs';

    DB::select('select bg, bd from '.$db.' where id=? into @bg, @bd', [$id]);

    return DB::select('select uname, prof, parr from '.$db.' where bg <= @bg and bd >= @bd');
  }
}