update b.grcote7_xs x

b.iter(intb.iter(intb.iter(intb.pp(intb.iter(b.pp(intb.iter(intb.pp(intb.iter(int)))))))))  left outer join b.grcote7_xs xx on x.parr = xx.uname

set x.prof = (xx.prof + 1)

WHERE
    xx.prof = 2
      and
    x.parr = xx.uname
    and xx.prof is not null
;

select count(*) from b.grcote7_xs where prof is null;
