BEGIN
SET @idec = idag;

SELECT
x.id, 

b.prenom,
b.email,
x.uname,
b.name AS nom,

b.user_enseigne,

if (b.user_adr2, CONCAT(b.user_adr, ' - ', b.user_adr2), user_adr) AS adr,

b.user_city,
if (b.user_cp, b.user_cp, NULL) AS cp,

b.user_country AS country,

b.user_int AS gender,

b.user_occ,
b.user_intrest,
b.url,

b.bio


FROM b.grcote7_xus x

	LEFT OUTER JOIN b.grcote7_boos_xus b
	ON b.id = x.uid

WHERE x.id=@idec

ORDER BY x.id

INTO @id, @prenom, @email, @uname, @nom, @company, @addr, @city, @zip, @country, @gender, @job, @about, @webpage, @acomment;


# UPDATE b.users SET NAME=@prenom, email=@email, is_activated=1, username=@uname, surname=@nom, company=@company, street_addr=@addr, city=@city, zip=@zip, country_id=@country, iu_gender=@gender, iu_job=@job, iu_about=@about, iu_webpage=@webpage, iu_comment=@acomment WHERE id=@idec;

#SELECT @id, @prenom, @email, @uname, @nom, @company, @addr, @city, @zip, @gender, @job, @about, @webpage, @acomment;
END
