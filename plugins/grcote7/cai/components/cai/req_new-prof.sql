SELECT
  x.uid,
  x.uname,
  x.prof,
  xx.uname as parr,
  (xx.prof + 1) as prof
FROM
  b.grcote7_xs x
  join b.grcote7_xs xx on x.parr = xx.uname
WHERE
  (
    x.parr = xx.uname
    and xx.prof is not null
  )
order by
  uid
