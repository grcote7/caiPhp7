<?php

/*
 * (c) Boosteur.com - 2020
 */

namespace Grcote7\Cai\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\DB;
use RainLab\User\Facades\Auth;

class Uu extends ComponentBase
{
  public $var;

  public function componentDetails()
  {
    return [
      'name'        => 'u',
      'description' => 'Manipes Users',
    ];
  }

  public function onRun()
  {
    ini_set('max_execution_time', 3600);
    error_reporting(E_ALL);
    // \Debugbar::enable();
    // $this->addCss('assets/css/dev.css');

    $id = Auth::getUser()->id ?? 5;

    // $this->var = $this->getCa();
    return $this->getActivGroup($id);
  }

  public function getActivGroup($id)
  {
    $db = 'grcote7_xus';
    $id = 6;
    // $parroris = [3, 6, 11, 12]; // Les parrori
    // $prof3 = range(1, 9); // Les prof3
    // $parroriTyp = array_unique(array_merge($parroris, $prof3));
    // sort($parroriTyp);
    // var_dump($parroris, $prof3, $parroriTyp);

    DB::select('select bg, bd, prof from '.$db.' where id=? into @bg, @bd, @prof', [$id]);

    $reqparroris = ' where parrori=?';
    $reqfill     = ' where bg >= @bg and bd <= @bd and (prof - @prof<3)';

    $parroris = DB::select('select id, uname from '.$db.$reqparroris.' order by bg', [$id]);
    $prof3    = DB::select('select id, uname from '.$db.$reqfill.' order by bg');

    return array_merge($parroris, ['-----'], $prof3);
    /*
    DB::select('select bg, bd, prof from '.$db.' where id=? into @bg, @bd, @prof', [1]);
    $req = ' where bg >= @bg and bd <= @bd ';

    $us = DB::select('select x.id, @prof as profini, uname, email, last_login, x.prof, parr from '.$db.' x	LEFT OUTER JOIN  users u
    ON x.id = u.id
    '.$req.' and (x.prof - @prof<3) order by bg');
    */
  // $email = 'dudu@cote7.com';
  // $parts = explode('@', $email);
  // return $parts[0];
/*
  $parts0 = [];

  foreach ($us as $k => $u) {
  $email    = $u->email;
  $parts    = explode('@', $email);
  $part     = $parts[0];
  $parts0[] = $u->id.' '.$part;

  // DB::update('update grcote7_boos_xus set email="'.$part.'@cote7.com" where email like "'.$part.'@clubadel.com%"');

  // echo $k.' : '.$email.' ('.$parts[0].')<br>';
  }

  $tab2 = array_unique($parts0);
  // echo count($parts0);

  return $parts0;
  // return $us;
  */
  }

  public function affGroup($id)
  {
    $gr = $this->getGroup($id);

    $affGr   = [];
    $affGr[] = '<table>';

    foreach ($gr as $u) {
      $affGr[] = '<tr><td>'.$u->prof.'</td><td>'.str_repeat('&nbsp;', (($u->prof - $u->profini) * 7)).' '.$u->uname.'</td><td>'.$u->email.'</td><td>'.(($u->last_login) ? 'Yes' : 'no').'</td></tr>';
    }
    $affGr[] = '</table>';

    return implode('', $affGr);
  }

  public function getGroup($id)
  {
    $db = 'grcote7_xus';

    DB::select('select bg, bd, prof from '.$db.' where id=? into @bg, @bd, @prof', [$id]);

    $req = ' where bg >= @bg and bd <= @bd ';

    return DB::select('select @prof as profini, uname, email, last_login, x.prof, parr from '.$db.' x	LEFT OUTER JOIN  users u
    ON x.id = u.id
  '.$req.' and (x.prof - @prof<4) order by bg');
  }

  public function getPeres($id)
  {
    $db = 'grcote7_xs';

    DB::select('select bg, bd from '.$db.' where id=? into @bg, @bd', [$id]);

    return DB::select('select uname, prof, parr from '.$db.' where bg <= @bg and bd >= @bd');
  }
}