<?php

/*
 * (c) Boosteur.com - 2020
 */

namespace Grcote7\Cai\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\DB;
use RainLab\User\Facades\Auth;

class Cai extends ComponentBase
{
    public $var;
    
  public function componentDetails()
  {
    return [
      'name'        => 'Cai Component',
      'description' => 'Page d\'accueil du C.A.I.',
    ];
  }

  public function defineProperties()
  {
    return [];
  }

  public function onRun()
  {
    // ini_set('max_execution_time', 3600);
    // error_reporting(E_ALL);
    // \Debugbar::enable();
    // $this->addCss('assets/css/dev.css');

    $id    = Auth::getUser()->id;
    // return DB::select('select lv, typ from '.$dtxus.' x where id=?', [$id]);

    $u = DB::select('select x.lv, x.typ, b.solde from grcote7_xus x left outer join grcote7_boos_xus b on b.id = x.id where x.id = ?', [$id]);
    
    // var_dump($u);
    // $u = DB::select('select lv, typ from '.$dtxus.' x where id=?', [$id]);

    // info($u);
    $this->var = $u[0];
    // return $this->var;
  // $var = $this->affGroup(3);
  // $this->var = $var;

  // return $this->group(1);
  }
}