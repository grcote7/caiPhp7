<?php

/*
 * (c) Boosteur.com - 2020
 */

namespace Grcote7\Cai\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\DB;
use RainLab\User\Facades\Auth;

class U extends ComponentBase
{
  public $var;

  public function componentDetails()
  {
    return [
      'name'        => 'u',
      'description' => 'Manipes Users',
    ];
  }

  public function onRun()
  {
    ini_set('max_execution_time', 3600);
    error_reporting(E_ALL);
    // \Debugbar::enable();
    // $this->addCss('assets/css/dev.css');

    $id = Auth::getUser()->id;

    $this->var = $this->affGroup($id);
  }

  public function affGroup($id)
  {
    $req = $this->getGroup($id);

    $info = $req[0];
    $gr   = $req[1];

    $affGr   = [];
    $affGr[] = '<table>';

    foreach ($gr as $u) {
      $affGr[] = '<tr><td>'.$u->prof.'</td><td>'.str_repeat('&nbsp;', (($u->prof - $u->profini) * 7)).' '.$u->uname.'</td><td>'.$u->email.'</td><td>'.(($u->last_login) ? 'Yes' : 'no').'</td></tr>';
    }
    $affGr[] = '</table>';

    // echo '<br>&nbsp;<br>&nbsp<br>&nbsp;';
    // var_dump($info[0]);

    $affGr[] = 'Nombre de membres: '.$info[0]->nb;

    return implode('', $affGr);
  }

  public function getGroup($id)
  {
    $db = 'grcote7_xus';

    DB::select('select bg, bd, prof from '.$db.' where id=? into @bg, @bd, @prof', [$id]);

    $req = ' where bg >= @bg and bd <= @bd ';

    $info = DB::select('select count(*) as nb from '.$db.' x	LEFT OUTER JOIN  users u
    ON x.id = u.id
  '.$req.' and (x.prof - @prof<3)');

    $gr = DB::select('select @prof as profini, uname, email, last_login, x.prof, parr from '.$db.' x	LEFT OUTER JOIN  users u
    ON x.id = u.id
  '.$req.' and (x.prof - @prof<3) order by bg');

    return [$info, $gr];
  }

  public function getPeres($id)
  {
    $db = 'grcote7_xs';

    DB::select('select bg, bd from '.$db.' where id=? into @bg, @bd', [$id]);

    return DB::select('select uname, prof, parr from '.$db.' where bg <= @bg and bd >= @bd');
  }
}