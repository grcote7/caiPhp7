<?php

/*
 * (c) Boosteur.com - 2020
 */

namespace Grcote7\User\Account\Components;

use Cms\Classes\ComponentBase;

class Account extends \RainLab\User\Components\Account
{
  public function componentDetails()
  {
    return [
      'name'        => 'B Account',
      'description' => 'Login process',
    ];
  }

  public function defineProperties()
  {
    return [];
  }
}