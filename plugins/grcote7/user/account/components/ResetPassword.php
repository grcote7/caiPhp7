<?php

/*
 * (c) Boosteur.com - 2020
 */

namespace Grcote7\User\Account\Components;

use ApplicationException;
use Lang;
use Mail;
use RainLab\User\Models\User as UserModel;
use ValidationException;
use Validator;

class ResetPassword extends \RainLab\User\Components\ResetPassword
{
  public function componentDetails()
  {
    return [
      'name'        => 'B ResetPassword',
      'description' => 'ResetPassword process',
    ];
  }

  public function defineProperties()
  {
    return [];
  }

  public function onRestorePassword()
  {
    $rules = [
      'email' => 'required|email|between:3,255',
    ];

    $validation = Validator::make(post(), $rules);
    if ($validation->fails()) {
      throw new ValidationException($validation);
    }

    $user = UserModel::findByEmail(post('email'));
    if (!$user || $user->is_guest) {
      throw new ApplicationException(Lang::get(/*A user was not found with the given credentials.*/'rainlab.user::lang.account.invalid_user'));
    }

    $code = implode('!', [$user->id, $user->getResetPasswordCode()]);

    $link = $this->makeResetUrl($code);

    $data = [
      'name'     => $user->name,
      'username' => $user->username,
      'link'     => $link,
      'code'     => $code,
    ];

    Mail::send('grcote7.user::mail.account.restore', $data, function ($message) use ($user) {
      $message->to($user->email, $user->full_name);
    });
  }
}