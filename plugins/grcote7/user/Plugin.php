<?php

/*
 * (c) Boosteur.com - 2020
 */

namespace Grcote7\User;

use Backend;
use System\Classes\PluginBase;

/**
 * user Plugin Information File.
 */
class Plugin extends PluginBase
{
  /**
   * Returns information about this plugin.
   *
   * @return array
   */
  public function pluginDetails()
  {
    // Pour mem
    // https://gist.github.com/LukeTowers/9100845ba50bd53d957da822d06f3b56
    // => composer.json et procédures diverses d'install

    return [
      'name'        => 'B User',
      'description' => 'Gestion des utilisateurs',
      'author'      => 'Grcote7',
      'icon'        => 'oc-icon-user',
    ];
  }

  /**
   * Register method, called when the plugin is first registered.
   */
  public function register()
  {
  }

  /**
   * Boot method, called right before the request route.
   *
   * @return array
   */
  public function boot()
  {
  }

  /**
   * Registers any front-end components implemented in this plugin.
   *
   * @return array
   */
  public function registerComponents()
  {
    return [
      'Grcote7\User\Components\User'                  => 'user',
      'Grcote7\User\Account\Components\Account'       => 'account',
      'Grcote7\User\Account\Components\ResetPassword' => 'resetpassword',
    ];
  }

  public function registerMailTemplates()
  {
    return [
      // 'rainlab.user::mail.activate',
      // 'rainlab.user::mail.welcome',
      'grcote7.user::mail.account.restore',
      // 'rainlab.user::mail.new_user',
      // 'rainlab.user::mail.reactivate',
      // 'rainlab.user::mail.invite',
    ];
  }

  /**
   * Registers any back-end permissions used by this plugin.
   *
   * @return array
   */
  public function registerPermissions()
  {
    return []; // Remove this line to activate
    return [
      'grcote7.user.some_permission' => [
        'tab'   => 'user',
        'label' => 'Some permission',
      ],
    ];
  }

  /**
   * Registers back-end navigation items for this plugin.
   *
   * @return array
   */
  public function registerNavigation()
  {
    return []; // Remove this line to activate
    return [
      'user' => [
        'label'       => 'B Users',
        'url'         => Backend::url('grcote7/user/mycontroller'),
        'icon'        => 'oc-icon-user',
        'permissions' => ['grcote7.user.*'],
        'order'       => 70,
      ],
    ];
  }
}