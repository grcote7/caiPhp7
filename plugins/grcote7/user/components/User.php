<?php

/*
 * (c) Boosteur.com - 2020
 */

namespace Grcote7\User\Components;

use Cms\Classes\ComponentBase;

class User extends ComponentBase
{
  public function componentDetails()
  {
    return [
      'name'        => 'B User',
      'description' => 'User process',
    ];
  }

  public function onRun()
  {
  }
}